import nodemailer from 'nodemailer'

// Your code here

async function sendEmail (req, res) {
  console.log('Hello')
  const transporter = nodemailer.createTransport({
    port: 587, ////comment
    host: 'smtp-mail.outlook.com',
    auth: {
      user: process.env.NODEMAILER_EMAIL,
      pass: process.env.NODEMAILER_PW
    }
  })

  const mailOptions = {
    from: 'process.env.NODEMAILER_EMAIL',
    to: 'abk51788@gmail.com, abdulbasit@ineffabledevs.com',
    subject: `Message From ${req.body.name}`,
    text: req.body.message + ' | Sent from: ' + req.body.email,
    html: `<div>${req.body.message}</div><div><p>Sent from: ${req.body.email}</p></div>`
  }

  try {
    const info = await transporter.sendMail(mailOptions)
    console.log('Email sent:', info.messageId)
    res.status(200).json({ success: true, message: 'Email sent successfully' })
  } catch (error) {
    console.error('Error sending email:', error)
    res.status(500).json({ success: false, error: 'Email sending failed' })
  }
}

export default sendEmail
