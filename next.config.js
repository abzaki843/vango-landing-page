/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    appDir: true
  }
}

module.exports = {
  images: {
    domains: [], // add your image domains if needed
    deviceSizes: [320, 420, 768, 1024, 1200], // adjust as needed
    imageSizes: [16, 32, 48, 64, 96] // adjust as needed
  }
}
