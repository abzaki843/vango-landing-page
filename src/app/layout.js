import './globals.css'

export const metadata = {
  title: 'Mover Buddy',
  description: 'A Freight Service App'
}

export default function RootLayout ({ children }) {
  return (
    <html lang='en'>
      <body>{children}</body>
    </html>
  )
}
