
import Image from 'next/image'
import { Poppins } from 'next/font/google'
import styles from './page.module.css'
import Header from '../../Components/Header'
import Navbar from '../../Components/Navbar'
import Services from '../../Components/Services'
import Work from '../../Components/Work'
import Description from '../../Components/Description'
import PlaceOrder from '../../Components/PlaceOrder'
import AboutUs from '../../Components/AboutUs'
import FAQ from '../../Components/FAQ'
import ContactUs from '../../Components/ContactUs'
import Footer from '../../Components/Footer'
const poppins = Poppins({
  weight: ['400', '500', '600', '700'],
  subsets: ['latin'],
});
export default function Home() {
  return (
    <div>
    <Navbar />
     <Header />
     <Services />
     <Work />
     <Description />
     <PlaceOrder />
     <AboutUs />
     <FAQ />
     <ContactUs />
     <Footer />
    </div>
  )
}
