'use client'
import React from 'react'
import Container from '@mui/material/Container'
import Image from 'next/image'
import useMediaQuery from '@mui/material/useMediaQuery'
import {
  Grid,
  Box,
  Typography,
  TextField,
  Button,
  Card,
  CardContent
} from '@mui/material'
const CompleteOrderSection = () => {
  const medium = useMediaQuery('(max-width:800px)')
  return (
    <>
      <Grid
        container
        direction='row'
        justifyContent={medium ? 'center ' : 'left'}
        alignItems={medium ? 'center ' : 'left'}
        textAlign='center'
      >
        <Grid item xl={5} lg={6} md={5} sm={5} xs={12}>
          <Box>
            <Image
              src='/Place order mockup.png'
              height={500}
              width={500}
              layout='responsive'
            />
          </Box>
        </Grid>
        <Grid
          item
          xl={5}
          lg={6}
          md={7}
          sm={7}
          xs={12}
          sx={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            textAlign: medium ? 'center' : 'left'
          }}
        >
          <Container>
            <Typography
              variant={medium ? 'h5' : 'h4'}
              sx={{ fontWeight: 'bold' }}
              mb={1}
              mt={3}
            >
              Complete Order{' '}
            </Typography>
            <Typography variant='body1' sx={{ textAlign: 'justify' }}>
              {' '}
              When driver upon reaching destination will update their status to
              reach destination a random generated code will to customer through
              email .Customer will send this code to Driver then Driver will
              enter this code to complete order making this whole process
              entirely save , secure & satisfactory{' '}
            </Typography>
          </Container>
        </Grid>
      </Grid>
    </>
  )
}

export default CompleteOrderSection
