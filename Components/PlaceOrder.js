'use client'
import React from 'react'
import PropTypes from 'prop-types'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'
import { styled } from '@mui/material/styles'
import Image from 'next/image'
import { useState } from 'react'
import Container from '@mui/material/Container'
import Payment from '../public/Payment.png'
import PlaceSection from './PlaceSection'
import PaymentSection from './PaymentSection'
import OrderTrackingSection from './OrderTrackingSection'
import CompleteOrderSection from './CompleteOrderSection'
import useMediaQuery from '@mui/material/useMediaQuery'
import {
  Grid,
  Box,
  Typography,
  TextField,
  Button,
  Card,
  CardContent
} from '@mui/material'

function TabPanel (props) {
  const { children, value, index, ...other } = props

  return (
    <div
      role='tabpanel'
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3, height: '100%' }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  )
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired
}

function a11yProps (index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`
  }
}

const PlaceOrder = () => {
  // const [value, setValue] = React.useState(0);
  const matches = useMediaQuery('(max-width:900px)')
  const [selectedTab, setSelectedTab] = useState(0)
  const transparentCircleStyle = {
    position: 'relative',
    borderRadius: '50%',
    backgroundColor: '#F0F0F0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '60px',
    height: '60px',
    marginRight: '12px'
  }
  const tabCircleStyle = index => {
    if (selectedTab === index) {
      return {
        ...transparentCircleStyle,
        backgroundColor: '#686A6C'
      }
    }
    return transparentCircleStyle
  }
  const handleChange = (event, newValue) => {
    setSelectedTab(newValue)
  }
  return (
    <>
      <Container maxWidth='xl'>
        <Grid
          container
          direction='row'
          justifyContent='center'
          alignItems='center'
          textAlign='center'
          mt={3}
        >
          <Grid item>
            <Typography variant='h4' sx={{ fontWeight: 'bold' }} mb={2} mt={3}>
              How Mover Buddy Works
            </Typography>
            <Typography variant='body1' mb={3}>
              Mover Buddy works as a digital platform that connects shippers
              (individuals or businesses looking to transport goods) with
              carriers (drivers, trucking companies, or logistics providers) to
              facilitate the transportation of cargo.
            </Typography>
          </Grid>
        </Grid>
        <Grid container>
          <Grid
            item
            lg={12}
            md={12}
            sm={12}
            xs={12}
            sx={{ display: matches ? null : 'flex' }}
          >
            {' '}
            <Box height={matches ? 120 : 400}>
              <Tabs
                orientation={matches ? 'horizontal' : 'vertical'}
                variant='scrollable'
                value={selectedTab}
                height='auto'
                onChange={handleChange}
                aria-label='Vertical tabs example'
                sx={{
                  // marginTop:6,

                  width: matches ? 'auto' : 300,
                  borderRight: 1,
                  borderColor: 'divider',

                  '& .Mui-selected .MuiTab-wrapper': {
                    color: 'blue' // change color of selected tab text
                  },
                  '& .MuiTab-root': {
                    fontSize: '13px',
                    fontWeight: 'bold'
                    //  paddingLeft: '20px',
                  },
                  '& .MuiTabs-indicator': {
                    width: '8px'
                  },
                  '& .MuiTabs-flexContainer': {
                    height: '100%'
                  },
                  '& .MuiTab-wrapper': {
                    whiteSpace: 'nowrap' // add this property
                  }
                }}
              >
                <Tab
                  label={
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                      {' '}
                      <div style={tabCircleStyle(0)}>
                        <Image
                          src={
                            selectedTab === 0
                              ? '/placewhite.png'
                              : '/Place order.png'
                          }
                          alt='icon2'
                          height='0'
                          width='0'
                          sizes='100vw'
                          style={{
                            width: '23px',
                            height: '35px',
                            objectFit: 'cover',
                            marginRight: 10,
                            display: 'block',
                            margin: 'auto'
                          }}
                        />
                      </div>{' '}
                      <span style={{ marginLeft: '6px' }}>Place Order</span>{' '}
                    </div>
                  }
                  {...a11yProps(0)}
                />
                <Tab
                  label={
                    <div
                      style={{
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        height: '80px',
                        marginRight: '45px'
                      }}
                    >
                      {' '}
                      <div style={tabCircleStyle(1)}>
                        <Image
                          src={
                            selectedTab === 1
                              ? '/Payment.png'
                              : '/paymentblack.png'
                          }
                          alt='icon2'
                          height='0'
                          width='0'
                          sizes='100vw'
                          style={{
                            width: '32px',
                            height: '28px',
                            objectFit: 'cover',
                            marginRight: 10,
                            display: 'block',
                            margin: 'auto'
                          }}
                        />
                      </div>{' '}
                      <span style={{ marginLeft: '6px' }}>Payment</span>{' '}
                    </div>
                  }
                  {...a11yProps(1)}
                />
                <Tab
                  label={
                    <div
                      style={{
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        height: '80px'
                      }}
                    >
                      {' '}
                      <div style={tabCircleStyle(2)}>
                        <Image
                          src={
                            selectedTab === 2
                              ? '/trackingwhite.png'
                              : '/Order Tracking.png'
                          }
                          alt='icon2'
                          height='0'
                          width='0'
                          sizes='100vw'
                          style={{
                            width: '37px',
                            height: '27px',
                            objectFit: 'cover',
                            marginRight: 8,
                            display: 'block',
                            margin: 'auto'
                          }}
                        />
                      </div>{' '}
                      <span style={{ marginLeft: '6px' }}>Order Tracking</span>{' '}
                    </div>
                  }
                  {...a11yProps(2)}
                />
                <Tab
                  label={
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                      <div style={tabCircleStyle(3)}>
                        {' '}
                        <Image
                          src={
                            selectedTab === 3
                              ? '/CompWhite.png'
                              : '/Complete Order.png'
                          }
                          alt='icon2'
                          height='0'
                          width='0'
                          sizes='100vw'
                          style={{
                            width: '29px',
                            height: '30px',
                            objectFit: 'cover',
                            marginRight: 8,
                            display: 'block',
                            margin: 'auto'
                          }}
                        />
                      </div>{' '}
                      <span style={{ marginLeft: '6px' }}>Complete Order</span>{' '}
                    </div>
                  }
                  {...a11yProps(3)}
                />
              </Tabs>
            </Box>
            <Box sx={{ height: 'auto', width: '100%' }}>
              <TabPanel value={selectedTab} index={0}>
                <PlaceSection />
              </TabPanel>
              <TabPanel value={selectedTab} index={1}>
                <PaymentSection />
              </TabPanel>
              <TabPanel value={selectedTab} index={2}>
                <OrderTrackingSection />
              </TabPanel>
              <TabPanel value={selectedTab} index={3}>
                <CompleteOrderSection />
              </TabPanel>
            </Box>
          </Grid>
        </Grid>
      </Container>
    </>
  )
}

export default PlaceOrder
