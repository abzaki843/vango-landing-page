'use client'
import React from 'react'
import Container from '@mui/material/Container'
import Image from 'next/image'
import useMediaQuery from '@mui/material/useMediaQuery'
import {
  Grid,
  Box,
  Typography,
  TextField,
  Button,
  Card,
  CardContent
} from '@mui/material'
const PaymentSection = () => {
  const medium = useMediaQuery('(max-width:600px)')
  return (
    <>
      <Grid
        container
        direction='row'
        justifyContent={medium ? 'center ' : 'left'}
        alignItems={medium ? 'center ' : 'left'}
        textAlign='center'
      >
        <Grid
          item
          xl={5}
          lg={5}
          md={5}
          sm={5}
          xs={12}
          sx={{ display: 'flex', flexDirection: 'row' }}
        >
          <Box>
            <Image
              src='/Place order mockup.png'
              height={200}
              width={300}
              layout='responsive'
            />
          </Box>
        </Grid>
        <Grid
          item
          xl={6}
          lg={6}
          md={7}
          sm={7}
          xs={12}
          sx={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            textAlign: medium ? 'center' : 'left'
          }}
        >
          <Container>
            <Typography variant='h4' sx={{ fontWeight: 'bold' }} mb={1} mt={3}>
              Payment
            </Typography>
            <Typography
              variant='body1'
              sx={{ textAlign: medium ? 'justify' : null }}
            >
              Paying for our freight services is hassle-free. After confirming
              your order, you can securely make payments through various
              methods, including credit/debit & Bank transfer . Your billing
              details are handled with utmost security and privacy. Once your
              shipment is successfully delivered we ll provide a detailed
              invoice for your review. Our user-friendly payment process ensures
              a seamless and trustworthy experience throughout your shipping
              journey.{' '}
            </Typography>
          </Container>
        </Grid>
      </Grid>
    </>
  )
}

export default PaymentSection
