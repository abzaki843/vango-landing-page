'use client'
import React from 'react'
import Container from '@mui/material/Container'
import Image from 'next/image'
import useMediaQuery from '@mui/material/useMediaQuery'
import {
  Grid,
  Box,
  Typography,
  TextField,
  Button,
  Card,
  CardContent
} from '@mui/material'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import StarIcon from '@mui/icons-material/Star'
const Work = () => {
  const matches = useMediaQuery('(max-width:900px)')
  return (
    <>
      {/* {matches ? ( */}

      <Grid
        container
        direction='row'
        justifyContent='center'
        alignItems='center'
        textAlign='center'
        mt={8}
        spacing={5}
      >
        <Grid item xl={8} lg={8} md={10} sm={10}>
          <Typography variant='h4' sx={{ fontWeight: 'bold' }} mt={2}>
            Work with Us
          </Typography>
        </Grid>
        <Grid item xs={11} sm={10} md={8} lg={6}>
          <Image src='/work.png' width={500} height={500} layout='responsive' />
        </Grid>
        <Grid item>
          <Container>
            <Typography
              variant='body1'
              mt={2}
              mb={2}
              sx={{ textAlign: 'justify' }}
            >
              We are excited to extend a warm invitation to all drivers and
              transportation companies to be a part of our cutting-edge online
              freight services app. As we embark on a journey ofn our dynamic
              platform, connecting drivers, carriers, and revolutionizing the
              logistics industry, we welcome you to joi companies for a seamless
              and efficient shipping experience.
            </Typography>
            <List
              sx={{
                width: '100%',
                maxWidth: 360,
                bgcolor: 'background.paper'
              }}
              aria-label='contacts'
            >
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemIcon>
                    <StarIcon />
                  </ListItemIcon>
                  <ListItemText primary='Endless Opportunities' />
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemIcon>
                    <StarIcon />
                  </ListItemIcon>
                  <ListItemText primary='Real-Time Updates' />
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemIcon>
                    <StarIcon />
                  </ListItemIcon>
                  <ListItemText primary='Flexible Working Options' />
                </ListItemButton>
              </ListItem>
              <ListItem disablePadding>
                <ListItemButton>
                  <ListItemIcon>
                    <StarIcon />
                  </ListItemIcon>
                  <ListItemText primary='24/7 Support' />
                </ListItemButton>
              </ListItem>
            </List>
            <Button
              sx={{
                backgroundColor: '#4336B7',
                color: 'white',
                height: 55,
                width: 245,
                borderRadius: 13,
                textTransform: 'none',
                '&:hover': {
                  backgroundColor: '#4336B7',
                  color: 'white'
                }
              }}
            >
              Become a Driver
            </Button>
          </Container>
        </Grid>
      </Grid>
    </>
  )
}

export default Work
