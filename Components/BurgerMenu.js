import * as React from 'react'
import { styled, useTheme } from '@mui/material/styles'
import Box from '@mui/material/Box'
import Drawer from '@mui/material/Drawer'
import CssBaseline from '@mui/material/CssBaseline'
import MuiAppBar from '@mui/material/AppBar'
import Toolbar from '@mui/material/Toolbar'
import List from '@mui/material/List'
import Typography from '@mui/material/Typography'
import IconButton from '@mui/material/IconButton'
import MenuIcon from '@mui/icons-material/Menu'
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft'
import ChevronRightIcon from '@mui/icons-material/ChevronRight'
import Image from 'next/image'
import { Button, Grid } from '@mui/material'
import Link from '@mui/material'
import { useRouter } from 'next/router'
import useMediaQuery from '@mui/material/useMediaQuery'
const drawerWidth = 200

const Main = styled('main', { shouldForwardProp: prop => prop !== 'open' })(
  ({ theme, open }) => ({
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    marginLeft: `-${drawerWidth}px`,
    ...(open && {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen
      }),
      marginLeft: 0
    })
  })
)

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: prop => prop !== 'open'
})(({ theme, open }) => ({
  transition: theme.transitions.create(['margin', 'width'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen
  }),
  ...(open && {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: `${drawerWidth}px`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen
    })
  })
}))

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: 'flex-end'
}))

export default function PersistentDrawerLeft () {
  const medium = useMediaQuery('(max-width:1100px)')
  const theme = useTheme()
  const [open, setOpen] = React.useState(false)

  const handleDrawerOpen = () => {
    setOpen(true)
  }

  const handleDrawerClose = () => {
    setOpen(false)
  }

  return (
    <Grid>
      <Box xs={12} md={12} sm={6} sx={{ backgroundColor: 'pink' }}>
        <CssBaseline />
        <AppBar
          position='absolute'
          style={{ backgroundColor: 'transparent', boxShadow: 'none' }}
          open={open}
        >
          <Toolbar>
            <Box
              sx={{
                display: 'flex',
                alignItems: 'center',
                flex: '1',
                color: 'black'
              }}
            >
              <IconButton
                color='inherit'
                aria-label='open drawer'
                onClick={handleDrawerOpen}
                edge='start'
                sx={{ mr: 2, ...(open && { display: 'none' }) }}
              >
                <MenuIcon />
              </IconButton>
            </Box>
            {/* <Image src="/logo.png" alt="logo" width={170} height={60} /> */}
            <Box sx={{ flex: '1', backgroundColor: 'pink' }}></Box>
          </Toolbar>
        </AppBar>
        <Drawer
          sx={{
            width: drawerWidth,
            color: 'black',

            flexShrink: 0,
            '& .MuiDrawer-paper': {
              width: drawerWidth,
              boxSizing: 'border-box'
            }
          }}
          variant='persistent'
          anchor='left'
          open={open}
        >
          <DrawerHeader>
            <IconButton
              onClick={handleDrawerClose}
              style={{ marginRight: '150px' }}
            >
              <ChevronRightIcon />
            </IconButton>
          </DrawerHeader>
          <Button sx={{ color: '#fff' }}>
            {' '}
            <span style={{ color: 'black' }}>Home</span>
          </Button>
          <Button sx={{ color: '#fff' }}>
            {' '}
            <span style={{ color: 'black' }}>Services</span>
          </Button>
          <Button sx={{ color: '#fff' }}>
            {' '}
            <span style={{ color: 'black' }}>Contact Us</span>
          </Button>
          <Button sx={{ color: '#fff' }}>
            {' '}
            <span style={{ color: 'black' }}>About Us</span>
          </Button>
        </Drawer>
      </Box>
    </Grid>
  )
}
