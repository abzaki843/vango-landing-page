'use client'
import React from 'react'
import Container from '@mui/material/Container'
import Image from 'next/image'
import {
  Grid,
  Box,
  Typography,
  TextField,
  Button,
  Card,
  CardContent
} from '@mui/material'
const Description = () => {
  return (
    <section id='Description'>
      <>
        <Grid
          container
          direction='row'
          justifyContent='center'
          alignItems='center'
          textAlign='center'
          mt={6}
          sx={{ backgroundColor: '#F3F8FF' }}
        >
          <Container maxWidth='xl'>
            <Grid item>
              <Typography
                variant='h4'
                sx={{ fontWeight: 'bold' }}
                mb={2}
                mt={3}
              >
                Why Choose Us
              </Typography>
              <Typography variant='body1'>
                Our Van freight service ensures efficient and reliable
                transportation of your cargo.
                <br /> We prioritize on-time deliveries, providing you with
                peace of mind and helping you meet your customers expectations.
              </Typography>
            </Grid>
            <Grid
              container
              direction='row'
              justifyContent='center'
              alignItems='left'
              textAlign='left'
              spacing={4}
              mt={2}
              mb={4}
            >
              <Grid item xl={3.5} lg={3.5} md={4} sm={12} xs={12}>
                <Card
                  sx={{
                    boxShadow: '0 2px 6px  rgba(0,0,0,0.25)',
                    borderRadius: 5
                  }}
                >
                  <Container>
                    <CardContent>
                      <Image
                        src='/Vehicle.png'
                        alt='header'
                        height='0'
                        width='0'
                        sizes='100vw'
                        style={{
                          width: '99px',
                          height: '85px',
                          objectFit: 'cover'
                        }}
                      />
                      <Typography
                        variant='h6'
                        mt={2}
                        sx={{ fontWeight: 'bold', fontSize: 18 }}
                      >
                        Chose Vehicle of your Choice
                      </Typography>
                      <Typography variant='body1' mt={1}>
                        Whether you need to move small packages, bulk shipments,
                        or specialized goods, our versatile vans are ready
                      </Typography>
                    </CardContent>
                  </Container>
                </Card>
              </Grid>
              <Grid item xl={3.5} lg={3.5} md={4} sm={12} xs={12}>
                <Card
                  sx={{
                    boxShadow: '0 2px 6px  rgba(0,0,0,0.25)',
                    borderRadius: 5
                  }}
                >
                  <Container>
                    <CardContent>
                      <Image
                        src='/Cost.png'
                        alt='header'
                        height='0'
                        width='0'
                        sizes='100vw'
                        style={{
                          width: '97px',
                          height: '95px',
                          objectFit: 'cover'
                        }}
                      />
                      <Typography
                        variant='h6'
                        mt={1}
                        sx={{ fontWeight: 'bold', fontSize: 18 }}
                      >
                        Save 50% Cost Of your Moving
                      </Typography>
                      <Typography variant='body1' mt={1}>
                        With Mover Buddy you can save your moving expenses
                        without compromising on quality and reliability.
                      </Typography>
                    </CardContent>
                  </Container>
                </Card>
              </Grid>
              <Grid item xl={3.5} lg={3.5} md={4} sm={12} xs={12}>
                <Card
                  sx={{
                    boxShadow: '0 2px 6px  rgba(0,0,0,0.25)',
                    borderRadius: 5
                  }}
                >
                  <Container>
                    <CardContent>
                      <Image
                        src='/Time.png'
                        alt='header'
                        height='0'
                        width='0'
                        sizes='100vw'
                        style={{
                          width: '95px',
                          height: '97px',
                          objectFit: 'cover'
                        }}
                      />
                      <Typography
                        variant='h6'
                        mt={1}
                        sx={{ fontWeight: 'bold', fontSize: 18 }}
                      >
                        Book a ride at a time that suits you
                      </Typography>
                      <Typography variant='body1' mt={1}>
                        Our Van freight service offers flexible scheduling
                        options to meet your unique shipping needs.
                      </Typography>
                    </CardContent>
                  </Container>
                </Card>
              </Grid>
            </Grid>
          </Container>
        </Grid>
      </>
    </section>
  )
}

export default Description
