'use client'
import React from 'react'
import Container from '@mui/material/Container'
import Image from 'next/image'
import Link from 'next/link'
import {
  Grid,
  Box,
  Typography,
  TextField,
  Button,
  Card,
  CardContent
} from '@mui/material'

const Footer = () => {
  const navItems = [
    { name: 'Our Services', path: '#Services' },
    { name: 'About Us', path: '#Description' },
    { name: ' FAQS', path: '#Faq' },
    { name: 'Contact  ', path: '#Contact' }
  ]
  return (
    <>
      <Grid
        container
        direction='row'
        justifyContent='space-around'
        textAlign='left'
        sx={{ backgroundColor: '#F3F8FF' }}
        spacing={3}
      >
        <Grid item xl={2} lg={2} md={3} sm={8} xs={8} mb={3}>
          <Box mt={1.7}>
            <Image src='/logoo.png' alt='header' height={50} width={150} />
          </Box>
          <Typography variant='body2' mt={1}>
            Elevate your shipping experience with MOVER BUDDY. We deliver
            excellence in freight services, ensuring secure transport, on-time
            arrivals,and dedicated customer care. Your satisfaction, our
            priority.
          </Typography>
        </Grid>
        <Grid item xl={2} lg={2} md={2} sm={8} xs={8}>
          <Typography variant='h5' mt={2} sx={{ fontWeight: 'bold' }}>
            Links
          </Typography>
          <Grid container direction='column' textAlign='left'>
            {navItems.map(item => (
              <Button
                key={item.path}
                sx={{ color: '#212121', textAlign: 'left', paddingRight: 17 }}
              >
                <Link
                  href={item.path}
                  style={{
                    textDecoration: 'none',
                    color: '#212121',
                    textTransform: 'none'
                  }}
                >
                  {item.name}
                </Link>
              </Button>
            ))}
          </Grid>
        </Grid>
        <Grid item xl={3} lg={3} md={3} sm={8} xs={8} mb={3}>
          <Typography variant='h5' mt={2} sx={{ fontWeight: 'bold' }}>
            <Link
              href={'#contact'}
              style={{
                textDecoration: 'none',
                color: '#212121',
                textTransform: 'none'
              }}
            >
              Contact Us
            </Link>
          </Typography>

          <Box sx={{ display: 'flex', justifyContent: 'left' }} mt={1}>
            <Image
              src='/Location.png'
              height='0'
              width='0'
              sizes='100vw'
              style={{ width: '20px', height: '20px', objectFit: 'cover' }}
            />
            <Box>
              <Typography variant='body2'>
                Office# 2 Bussiness Incubation Center <br /> Comsats Islamabad,
              </Typography>
              <Typography variant='body2'>Islamabad Pakistan </Typography>
            </Box>
          </Box>
          <Box sx={{ display: 'flex', justifyContent: 'left' }} mt={1}>
            <Image
              src='/Phone.png'
              height='0'
              width='0'
              sizes='100vw'
              style={{ width: '20px', height: '20px', objectFit: 'cover' }}
            />

            <Typography variant='body2' ml={0.3}>
              Phone # 0992-3365551788
            </Typography>
          </Box>
        </Grid>
        <Grid item xl={3} lg={3} md={3} sm={8} xs={8} mb={2}>
          <Typography variant='h5' mt={2} sx={{ fontWeight: 'bold' }}>
            Follow Us
          </Typography>
          <Box sx={{ display: 'flex' }} mt={2}>
            {/* <Box sx={{height:50, width: 30, backgroundColor:"white", alignItems:"center", textAlign:"center"}}> */}
            <Image
              src='/Twitter.png'
              height='0'
              width='0'
              sizes='100vw'
              style={{ width: '25px', height: '22px', objectFit: 'cover' }}
            />

            <Image
              src='/linkedin.png'
              height='0'
              width='0'
              sizes='100vw'
              style={{
                width: '24px',
                height: '21px',
                objectFit: 'cover',
                marginLeft: 17
              }}
            />
            <Image
              src='/Instagram.png'
              height='0'
              width='0'
              sizes='100vw'
              style={{
                width: '20px',
                height: '21px',
                objectFit: 'cover',
                marginLeft: 20
              }}
            />
            <Image
              src='/Facebook.png'
              height='0'
              width='0'
              sizes='100vw'
              style={{
                width: '10px',
                height: '20px',
                objectFit: 'cover',
                marginLeft: 19
              }}
            />
          </Box>
        </Grid>
      </Grid>
    </>
  )
}

export default Footer
