'use client'
import React from 'react'
import Container from '@mui/material/Container'
import Image from 'next/image'
import {
  Grid,
  Box,
  Typography,
  TextField,
  Button,
  Card,
  CardContent
} from '@mui/material'
const Services = () => {
  return (
    <Container maxWidth='xl' id='Services'>
      <Grid
        container
        direction='row'
        justifyContent='center'
        alignItems='center'
        textAlign='center'
      >
        <Grid item mt={3} mb={3}>
          <Typography variant='h4' sx={{ fontWeight: 'bold' }}>
            Our Services
          </Typography>
          <Typography variant='h6' mt={1}>
            Place an Order for Freight Shipping while leaving all the hassle to
            us
          </Typography>
        </Grid>
      </Grid>
      <Grid
        container
        direction='row'
        justifyContent='center'
        alignItems='left'
        textAlign='left'
        spacing={2}
        mb={3}
      >
        <Grid item xl={3} lg={3} md={5} sm={5} xs={12}>
          <Card
            sx={{ boxShadow: '0 3px 6px 0 rgba(0,0,0,0.2)', borderRadius: 5 }}
          >
            {/* commented this
             */}
            <Container>
              <CardContent>
                <Image
                  src='/Store.png'
                  alt='header'
                  height='0'
                  width='0'
                  sizes='100vw'
                  style={{ width: '110px', height: '90px', objectFit: 'cover' }}
                />
                <Typography variant='h6' mt={1} sx={{ fontWeight: 'bold' }}>
                  Retail Store Moves
                </Typography>
                <Typography variant='body2'>
                  We optimize your inventory management, ensuring your shelves
                  are stocked with the right products at the right time.
                </Typography>
              </CardContent>
            </Container>
          </Card>
        </Grid>
        <Grid item xl={3} lg={3} md={5} sm={5} xs={12}>
          <Card
            sx={{ boxShadow: '0 3px 6px 0 rgba(0,0,0,0.2)', borderRadius: 5 }}
          >
            <Container>
              <CardContent>
                <Image
                  src='/Appartment Moves.png'
                  alt='header'
                  height='0'
                  width='0'
                  sizes='100vw'
                  style={{ width: '110px', height: '90px', objectFit: 'cover' }}
                />
                <Typography variant='h6' mt={1} sx={{ fontWeight: 'bold' }}>
                  Appartment Moves
                </Typography>
                <Typography variant='body2'>
                  We handle the logistics, heavy lifting, and transportation,
                  allowing you to focus on settling into your new space.
                </Typography>
              </CardContent>
            </Container>
          </Card>
        </Grid>
        <Grid item xl={3} lg={3} md={5} sm={5} xs={12}>
          <Card
            sx={{ boxShadow: '0 3px 6px 0 rgba(0,0,0,0.2)', borderRadius: 5 }}
          >
            <Container>
              <CardContent>
                <Image
                  src='/FurnitureMoves.png'
                  alt='header'
                  height='0'
                  width='0'
                  sizes='100vw'
                  style={{ width: '110px', height: '90px', objectFit: 'cover' }}
                />
                <Typography variant='h6' mt={1} sx={{ fontWeight: 'bold' }}>
                  Furniture Moves
                </Typography>
                <Typography variant='body2'>
                  We prioritize safeguarding your possessions with top-notch
                  materials for security and durability.
                </Typography>
              </CardContent>
            </Container>
          </Card>
        </Grid>
        <Grid item xl={3} lg={3} md={5} sm={5} xs={12}>
          <Card
            sx={{ boxShadow: '0 3px 6px 0 rgba(0,0,0,0.2)', borderRadius: 5 }}
          >
            <Container>
              <CardContent>
                <Image
                  src='/Business Moves.png'
                  alt='header'
                  height='0'
                  width='0'
                  sizes='100vw'
                  style={{ width: '110px', height: '90px', objectFit: 'cover' }}
                />
                <Typography variant='h6' mt={1} sx={{ fontWeight: 'bold' }}>
                  Business Moves
                </Typography>
                <Typography variant='body2'>
                  Our freight service safeguards possessions with premium
                  materials tailored for businesses
                </Typography>
              </CardContent>
            </Container>
          </Card>
        </Grid>
      </Grid>
    </Container>
  )
}

export default Services
