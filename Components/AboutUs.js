'use client'
import React from 'react'
import Container from '@mui/material/Container'
import Image from 'next/image'
import useMediaQuery from '@mui/material/useMediaQuery'
import {
  Grid,
  Box,
  Typography,
  TextField,
  Button,
  Card,
  CardContent
} from '@mui/material'
const AboutUs = () => {
  const medium = useMediaQuery('(max-width:900px)')
  return (
    <>
      {medium ? (
        <Container maxWidth='xl'>
          <Grid
            container
            direction='column'
            justifyContent='center'
            alignItems='center'
            textAlign='center'
            mt={2}
            spacing={4}
          >
            <Grid item xl={5.5} lg={5.5} md={6} sm={8} xs={8}>
              <Typography variant='h4' mb={2} sx={{ fontWeight: 'bold' }}>
                About Us
              </Typography>
            </Grid>
            <Grid item lg={6} md={7} sm={6} xs={8} mb={3}>
              <Box>
                <Image
                  src='/About.png'
                  height={200}
                  width={300}
                  layout='responsive'
                />
              </Box>
            </Grid>
            <Grid item mb={4}>
              <Typography variant='body2'>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industrys standard dummy text
                ever since the 1500s, when an unknown printer took a galley of
                type and scrambled it to make a type specimen book. It has
                survived not only five centuries, but also the leap into
                electronic typesetting, remaining essentially unchanged. It was
                popularised in the 1960s with the release of Letraset sheets
                containing Lorem Ipsum passages, and more recently with desktop
                publishing software like Aldus PageMaker including versions of
                Lorem Ipsum
              </Typography>
            </Grid>
          </Grid>
        </Container>
      ) : (
        <Container maxWidth='xl'>
          <Grid
            container
            direction='row'
            justifyContent='center'
            alignItems='left'
            textAlign='left'
            mt={2}
            spacing={4}
            mb={5}
          >
            <Grid
              item
              xl={5.5}
              lg={5.5}
              md={6}
              sm={12}
              xs={12}
              sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'left',
                justifyContent: 'center',
                textAlign: 'left'
              }}
            >
              <Typography variant='h4' mb={2} sx={{ fontWeight: 'bold' }}>
                About Us
              </Typography>
              <Typography variant='body1'>
                Mover Buddy is Freight Shipping Company based in the UK. We
                provide Cost-Effective and Safe Freight Shipping to our
                customers. Our Firm provide outstanding freight shipping
                solutions for all kinds of businesses and individuals. Inspired
                by Uber and InDriver we decided to make a mix of both
                applications into a simple yet highly interactive and easy to
                use application for Freight Shipping. We wanted to give our
                customers another option in Freight Shipping which is fully
                digital and easy to use all the while being more workable in a
                way that the customer can freely add their own bid.
              </Typography>
            </Grid>
            <Grid item xl={6} lg={6} md={7} sm={8} xs={8} mb={3}>
              <Box>
                <Image
                  src='/About.png'
                  height={200}
                  width={300}
                  layout='responsive'
                />
              </Box>
            </Grid>
          </Grid>
        </Container>
      )}
    </>
  )
}

export default AboutUs
