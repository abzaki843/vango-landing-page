'use client'
import React from 'react'
import Container from '@mui/material/Container'
import Image from 'next/image'
import useMediaQuery from '@mui/material/useMediaQuery'
import {
  Grid,
  Box,
  Typography,
  TextField,
  Button,
  Card,
  CardContent
} from '@mui/material'
const OrderTrackingSection = () => {
  const medium = useMediaQuery('(max-width:600px)')
  return (
    <>
      <Grid
        container
        direction='row'
        justifyContent={medium ? 'center ' : 'left'}
        alignItems={medium ? 'center ' : 'left'}
        textAlign='center'
      >
        <Grid
          item
          xl={5}
          lg={5}
          md={5}
          sm={5}
          xs={12}
          sx={{ display: 'flex', flexDirection: 'row' }}
        >
          <Box>
            <Image
              src='/Place order mockup.png'
              height={200}
              width={300}
              layout='responsive'
            />
          </Box>
        </Grid>
        <Grid
          item
          xl={6}
          lg={6}
          md={7}
          sm={7}
          xs={12}
          sx={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            textAlign: medium ? 'center' : 'left'
          }}
        >
          <Container>
            <Typography variant='h4' sx={{ fontWeight: 'bold' }} mb={1} mt={3}>
              Order Tracking
            </Typography>
            <Typography
              variant='body1'
              sx={{ textAlign: medium ? 'justify' : null }}
            >
              With our tracking feature youre always in the loop. Upon
              confirming your order youll receive real-time updates on your
              shipments location and status. Simply log in to the app to access
              detailed tracking information, estimated delivery times, and any
              potential delays. We keep you informed at every step, from pickup
              to drop-off. Our intuitive tracking system ensures you have full
              visibility and peace of mind throughout the entire shipping
              process, making your experience with our freight services app as
              smooth and reliable as possible.{' '}
            </Typography>
          </Container>
        </Grid>
      </Grid>
    </>
  )
}

export default OrderTrackingSection
