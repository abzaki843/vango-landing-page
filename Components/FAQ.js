'use client'
import React from 'react'
import Container from '@mui/material/Container'
import Image from 'next/image'
import { useState } from 'react'
import Accordion from '@mui/material/Accordion'
import AccordionSummary from '@mui/material/AccordionSummary'
import AccordionDetails from '@mui/material/AccordionDetails'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import AddIcon from '@mui/icons-material/Add'
import RemoveIcon from '@mui/icons-material/Remove'
import {
  Grid,
  Box,
  Typography,
  TextField,
  Button,
  Card,
  CardContent
} from '@mui/material'

const FAQ = () => {
  const [expanded, setExpanded] = useState(false)
  // const [expandedTwo, setExpandedTwo] = useState(false);
  // const [expandedThree, setExpandedThree] = useState(false);
  // const [expandedFour, setExpandedFour] = useState(false);
  const handleAccordionChange = panel => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : '')
  }

  const summaryStyle = panel => ({
    color: expanded === panel ? '#4336B7' : 'black',
    '& .MuiAccordionSummary-expandIcon': {
      color: expanded === panel ? '#4336B7' : 'inherit'
    }
  })
  return (
    <section id='Faq'>
      <>
        <Grid
          container
          direction='row'
          justifyContent='center'
          alignItems='center'
          textAlign='center'
          sx={{ backgroundColor: '#F3F8FF' }}
          mb={4}
        >
          <Grid item mt={3} mb={4}>
            <Typography variant='h5' sx={{ fontWeight: 'bold' }}>
              Frequently Asked Questions
            </Typography>
          </Grid>
          <Grid
            container
            direction='row'
            justifyContent='center'
            alignItems='center'
            textAlign='center'
          >
            <Grid item xl={8} lg={8} md={8} sm={10} xs={10} mb={7}>
              <Accordion
                sx={{ borderRadius: 2 }}
                expanded={expanded === 1}
                onChange={handleAccordionChange(1)}
              >
                <AccordionSummary
                  aria-controls='panel1a-content'
                  id='panel1a-header'
                  expandIcon={expanded === 1 ? <RemoveIcon /> : <AddIcon />}
                  style={summaryStyle(1)}
                >
                  <Typography variant='body1' sx={{ fontWeight: 'bold' }}>
                    What Type of Vehicles are you providing?
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Typography variant='body2' sx={{ textAlign: 'left' }}>
                    We provide customers with 4 transit options which are as
                    follows
                    <Typography variant='body2'> 1) Small Vans</Typography>
                    <Typography variant='body2'>2) Medium Vans</Typography>
                    <Typography variant='body2'> 3)large Vans</Typography>
                    <Typography variant='body2'> 4) Extra Vans</Typography>
                  </Typography>
                </AccordionDetails>
              </Accordion>
              <Accordion
                sx={{ borderRadius: 2, marginTop: 3 }}
                expanded={expanded === 2}
                onChange={handleAccordionChange(2)}
              >
                <AccordionSummary
                  aria-controls='panel1a-content'
                  id='panel1a-header'
                  expandIcon={expanded === 2 ? <RemoveIcon /> : <AddIcon />}
                  style={summaryStyle(2)}
                >
                  <Typography variant='body1' sx={{ fontWeight: 'bold' }}>
                    How do I become a Driver?
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Box m={2} />
                  <Typography variant='body1' sx={{ textAlign: 'left' }}>
                    To join our freight app as a driver, follow these steps:
                    <Typography variant='body2'>
                      <Box m={2} />
                      1) Download the app from your app store.
                    </Typography>
                    <Typography variant='body2'>
                      2) Sign up and create a driver profile{' '}
                    </Typography>
                    <Typography variant='body2'>
                      3) Complete identity and documentation verification.
                    </Typography>
                    <Typography variant='body2'>
                      4) Review and accept our terms. Start receiving and
                      accepting freight requests, and embark on your journey as
                      a driver for our platform!
                    </Typography>
                  </Typography>
                </AccordionDetails>
              </Accordion>
              <Accordion
                sx={{ borderRadius: 2, marginTop: 3 }}
                expanded={expanded === 3}
                onChange={handleAccordionChange(3)}
              >
                <AccordionSummary
                  aria-controls='panel1a-content'
                  id='panel1a-header'
                  expandIcon={expanded === 3 ? <RemoveIcon /> : <AddIcon />}
                  style={summaryStyle(3)}
                >
                  <Typography variant='body1' sx={{ fontWeight: 'bold' }}>
                    How do I contact Customer?
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Typography variant='body2' sx={{ textAlign: 'left' }}>
                    Drivers & Customers can intreract through in app chatting
                  </Typography>
                </AccordionDetails>
              </Accordion>
              <Accordion
                sx={{ borderRadius: 2, marginTop: 3 }}
                expanded={expanded === 4}
                onChange={handleAccordionChange(4)}
              >
                <AccordionSummary
                  aria-controls='panel1a-content'
                  id='panel1a-header'
                  expandIcon={expanded === 4 ? <RemoveIcon /> : <AddIcon />}
                  style={summaryStyle(4)}
                >
                  <Typography variant='body1' sx={{ fontWeight: 'bold' }}>
                    Mover Buddy is operation in which region
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Typography variant='body2' sx={{ textAlign: 'left' }}>
                    For now Mover Buddy is operational in UK & would be
                    operational in Pakistan as well
                  </Typography>
                </AccordionDetails>
              </Accordion>
            </Grid>
          </Grid>
        </Grid>
      </>
    </section>
  )
}

export default FAQ
