'use client'
import * as React from 'react'
import AppBar from '@mui/material/AppBar'
import Box from '@mui/material/Box'
import Toolbar from '@mui/material/Toolbar'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'
import Container from '@mui/material/Container'
import BurgerMenu from '../Components/BurgerMenu'
import Link from 'next/link'
import Image from 'next/image'
import ContactUs from './ContactUs'
import FAQ from './FAQ'
import Description from './Description'
import Services from './Services'
import { useMediaQuery } from '@mui/material'
const drawerWidth = 240
const navItems = [
  { name: 'Our Services', path: '#Services' },
  { name: 'About Us', path: '#Description' },
  { name: ' FAQS', path: '#Faq' },
  { name: 'Contact  ', path: '#Contact' }
]
const Appbar = () => {
  const medium = useMediaQuery('(max-width:950px)')
  return (
    <>
      {medium ? (
        <BurgerMenu />
      ) : (
        <div>
          <Box
            sx={{ flexGrow: 1, position: 'absolute', width: '95%' }}
            ml={2}
            mr={2}
          >
            <AppBar
              position='static'
              color='transparent'
              elevation={0.5}
              component='nav'
            >
              <Toolbar sx={{ justifyContent: 'space-between', ml: 2 }}>
                <Image
                  src='/logoo.png'
                  alt='hero'
                  //    height='0'
                  //                   width='0'
                  //                   sizes="100vw" style={{width:'900px', height:'400px'}}
                  height={50}
                  width={150}
                />
                <Box sx={{ display: { xs: 'none', sm: 'block' }, mr: 4 }}>
                  {navItems.map(item => (
                    <Button key={item.path} sx={{ color: '#212121' }}>
                      <Link
                        href={item.path}
                        style={{
                          textDecoration: 'none',
                          color: '#212121',
                          textTransform: 'none'
                        }}
                      >
                        {item.name}
                      </Link>
                    </Button>
                  ))}
                </Box>
              </Toolbar>
            </AppBar>
          </Box>
        </div>
      )}
    </>
  )
}
export default Appbar
