'use client'
import React from 'react'
import { useState } from 'react'
import Container from '@mui/material/Container'
import Image from 'next/image'
import Head from 'next/head'
import {
  Grid,
  Box,
  Typography,
  TextField,
  Button,
  Card,
  CardContent
} from '@mui/material'
import useMediaQuery from '@mui/material/useMediaQuery'

const ContactUs = () => {
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [message, setMessage] = useState('')
  const [submitted, setSubmitted] = useState(false)
  const medium = useMediaQuery('(max-width:900px)')
  const handleSubmit = e => {
    e.preventDefault()
    console.log('Sending')

    let data = {
      name,
      email,
      message
    }

    fetch('api/sendEmail.js', {
      method: 'POST',
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then(res => {
      console.log('Response received', res)
      if (res.status === 200) {
        console.log('Response succeeded!')
        setSubmitted(true)
        setName('')
        setEmail('')
        setMessage('')
      }
    })
  }
  return (
    <section id='Contact'>
      <>
        {medium ? (
          <Container>
            <Grid
              container
              direction='column'
              justifyContent='center'
              alignItems='center'
              textAlign='center'
              spacing={3}
              mb={4}
            >
              <Grid item xl={6} lg={6} md={6} sm={7} xs={7}>
                <Container>
                  <Box mb={3}>
                    <Typography
                      variant='h5'
                      sx={{ fontWeight: 'bold' }}
                      mt={2}
                      mb={3}
                    >
                      Get In Touch
                    </Typography>
                    <Image
                      src='/Contact.png'
                      height={300}
                      width={300}
                      layout='responsive'
                    />
                  </Box>
                </Container>
              </Grid>
              <Grid item xl={6} lg={6} md={6} sm={12} xs={12}>
                <Container>
                  <Typography variant='h6' mt={1} mb={2}>
                    Lets Start something new! Just ask and get answers.
                  </Typography>
                  <TextField
                    id='outlined-basic'
                    fullWidth
                    label='Name'
                    variant='outlined'
                    onChange={e => {
                      setName(e.target.value)
                    }}
                  />
                  <TextField
                    id='outlined-basic'
                    fullWidth
                    label='Email'
                    variant='outlined'
                    sx={{ marginTop: 2 }}
                    onChange={e => {
                      setEmail(e.target.value)
                    }}
                  />
                  <TextField
                    id='outlined-basic'
                    fullWidth
                    label='Message'
                    variant='outlined'
                    sx={{ marginTop: 2, marginBottom: 2 }}
                    multiline
                    rows={3}
                    onChange={e => {
                      setMessage(e.target.value)
                    }}
                  />
                  <Box mb={2}>
                    <Button
                      sx={{
                        backgroundColor: '#4336B7',
                        color: 'white',
                        '&:hover': {
                          backgroundColor: '#4336B7',
                          color: 'white'
                        },
                        height: 40,
                        width: 120
                      }}
                    >
                      Send
                    </Button>
                  </Box>
                </Container>
              </Grid>
            </Grid>
          </Container>
        ) : (
          <Container>
            <Grid
              container
              direction='row'
              justifyContent='center'
              alignItems='center'
              textAlign='left'
              spacing={3}
              mb={7}
            >
              <Grid item xl={6} lg={6} md={6} sm={12} xs={12}>
                <Container>
                  <Box mb={3}>
                    <Image
                      src='/Contact.png'
                      height={300}
                      width={300}
                      layout='responsive'
                    />
                  </Box>
                </Container>
              </Grid>
              <Grid item xl={6} lg={6} md={6} sm={12} xs={12}>
                <Container>
                  <Typography variant='h5' sx={{ fontWeight: 'bold' }} mt={2}>
                    Get In Touch
                  </Typography>
                  <Typography variant='body2' mt={1} mb={2}>
                    Lets Start something new! Just ask and get answers.
                  </Typography>
                  <TextField
                    id='outlined-basic'
                    fullWidth
                    label='Name'
                    variant='outlined'
                  />
                  <TextField
                    id='outlined-basic'
                    fullWidth
                    label='Email'
                    variant='outlined'
                    sx={{ marginTop: 2 }}
                  />
                  <TextField
                    id='outlined-basic'
                    fullWidth
                    label='Message'
                    variant='outlined'
                    sx={{ marginTop: 2, marginBottom: 2 }}
                    multiline
                    rows={3}
                  />
                  <Box mb={2}>
                    <Button
                      sx={{
                        backgroundColor: '#4336B7',
                        color: 'white',
                        '&:hover': {
                          backgroundColor: '#4336B7',
                          color: 'white'
                        },
                        height: 40,
                        width: 120
                      }}
                      onClick={handleSubmit}
                    >
                      Send
                    </Button>
                  </Box>
                </Container>
              </Grid>
            </Grid>
          </Container>
        )}
      </>
    </section>
  )
}

export default ContactUs
