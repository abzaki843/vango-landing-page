'use client'
import React from 'react'

import Image from 'next/image'
import { Grid, Box, Typography, TextField, Button } from '@mui/material'
import Container from '@mui/material/Container'
import useMediaQuery from '@mui/material/useMediaQuery'
const Header = () => {
  const medium = useMediaQuery('(min-width:560px) and (max-width:900px)')
  const small = useMediaQuery('(min-width:300px) and (max-width:559px)')
  return (
    <>
      {medium || small ? (
        <Grid
          container
          direction='column'
          justifyContent='center'
          alignItems='center'
          textAlign='center'
          sx={{ backgroundColor: '#F3F8FF' }}
        >
          <Grid
            item
            xl={6}
            lg={6}
            md={5}
            sm={12}
            xs={12}
            sx={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              textAlign: 'center',
              justifyContent: 'center'
            }}
            mt={6}
          >
            <Container>
              <Typography
                variant='h5'
                sx={{
                  color: '#4336B7',
                  fontWeight: '600',
                  fontFamily: 'Poppins'
                }}
              >
                {' '}
                Get your Things Moved easily and quickly{' '}
              </Typography>
              <Typography
                variant='h5'
                sx={{
                  color: '#4336B7',
                  fontWeight: '600',
                  fontFamily: 'Poppins'
                }}
              >
                {' '}
                in a cost-effective manner.
              </Typography>
            </Container>
          </Grid>
          <Grid item xl={6} lg={6} md={7} sm={5} xs={5} mt={3}>
            <Container>
              <Image
                src='/hero.png'
                alt='hero'
                //    height='0'
                //                   width='0'
                //                   sizes="100vw" style={{width:'900px', height:'400px'}}
                height={300}
                width={300}
                layout='responsive'
              />
            </Container>
          </Grid>
          <Grid item>
            <Typography variant='body1' mt={2}>
              Versatile Goods Shipment Service on Demand{' '}
            </Typography>
            <Typography variant='body1'>
              We are experts in quick & cost-effective Freight Shipping.{' '}
            </Typography>
            <Box
              mt={3}
              mb={6}
              sx={{
                display: 'flex',
                flexDirection: small ? 'column' : 'row',
                justifyContent: small ? 'center' : null,
                alignItems: small ? 'center' : null
              }}
            >
              <a
                href='https://app.moverbuddy.io/sign_up'
                target='_blank'
                rel='noopener noreferrer'
              >
                <Button
                  sx={{
                    backgroundColor: '#4336B7',
                    color: 'white',
                    height: 55,
                    width: 225,
                    borderRadius: 13,
                    textTransform: 'none',
                    '&:hover': {
                      backgroundColor: '#4336B7',
                      color: 'white'
                    }
                  }}
                >
                  Download App
                </Button>
              </a>
              <a
                href='https://driver.moverbuddy.io/sign_up'
                target='_blank'
                rel='noopener noreferrer'
              >
                <Button
                  sx={{
                    backgroundColor: '#D6D6D6',
                    color: 'black',
                    height: 55,
                    width: 225,
                    borderRadius: 13,
                    marginTop: small ? 2 : 0,
                    textTransform: 'none',
                    '&:hover': {
                      backgroundColor: '#D6D6D6',
                      color: 'black'
                    }
                  }}
                >
                  Become a Driver
                </Button>
              </a>
            </Box>
          </Grid>
        </Grid>
      ) : (
        <Grid
          container
          direction='row'
          justifyContent='center'
          alignItems='center'
          sx={{ backgroundColor: '#F3F8FF' }}
        >
          <Grid
            item
            xl={6}
            lg={6}
            md={5}
            sm={12}
            xs={12}
            sx={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'left'
            }}
            mt={12}
            mb={medium ? 0 : 8}
          >
            <Container>
              <Typography
                variant='h2'
                sx={{
                  color: '#4336B7',
                  fontWeight: '600',
                  fontFamily: 'Poppins'
                }}
              >
                {' '}
                Get your Things Moved Easily & Quickly
              </Typography>
              {/* <Typography
                variant='h3'
                sx={{
                  color: '#4336B7',
                  fontWeight: '600',
                  fontFamily: 'Poppins'
                }}
              >
                in a cost-effective manner.
              </Typography> */}
              <Typography variant='body1' mt={2}>
                Versatile Goods Shipment Service on Demand{' '}
              </Typography>
              <Typography variant='body1'>
                We are experts in quick & cost-effective Freight Shipping.{' '}
              </Typography>
              <Box mt={3} mb={3} sx={{ display: 'flex' }}>
                <a
                  href='https://app.moverbuddy.io/sign_up'
                  target='_blank'
                  rel='noopener noreferrer'
                >
                  <Button
                    sx={{
                      backgroundColor: '#4336B7',
                      color: 'white',
                      height: 55,
                      width: 225,
                      borderRadius: 13,
                      textTransform: 'none',
                      '&:hover': {
                        backgroundColor: '#4336B7',
                        color: 'white'
                      }
                    }}
                  >
                    Download App
                  </Button>
                </a>
                <Box m={1} />
                <a
                  href='https://driver.moverbuddy.io/sign_up'
                  target='_blank'
                  rel='noopener noreferrer'
                >
                  <Button
                    sx={{
                      backgroundColor: '#D6D6D6',
                      color: 'black',
                      height: 55,
                      width: 225,
                      borderRadius: 13,
                      marginLeft: 2,
                      textTransform: 'none',
                      '&:hover': {
                        backgroundColor: '#D6D6D6',
                        color: 'black'
                      }
                    }}
                  >
                    Become a Driver
                  </Button>
                </a>
              </Box>
            </Container>
          </Grid>
          <Grid
            item
            xl={5}
            lg={5}
            md={7}
            sm={12}
            xs={12}
            mt={medium ? 3 : 15}
            mb={medium ? 0 : 8}
          >
            <Container>
              <Image
                src='/hero.png'
                alt='hero'
                //    height='0'
                //                   width='0'
                //                   sizes="100vw" style={{width:'900px', height:'400px'}}
                height={300}
                width={300}
                layout='responsive'
              />
            </Container>
          </Grid>
        </Grid>
      )}
    </>
  )
}

export default Header
