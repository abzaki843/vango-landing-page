'use client'
import React from 'react'
import Container from '@mui/material/Container'
import Image from 'next/image'
import useMediaQuery from '@mui/material/useMediaQuery'
import {
  Grid,
  Box,
  Typography,
  TextField,
  Button,
  Card,
  CardContent
} from '@mui/material'
const PlaceSection = () => {
  const medium = useMediaQuery('(max-width:600px)')
  return (
    <>
      <Grid
        container
        direction='row'
        justifyContent='left'
        alignItems='left'
        textAlign='center'
      >
        <Grid item xl={5} lg={5} md={5} sm={5} xs={12}>
          <Image
            src='/Place order mockup.png'
            height={500}
            width={500}
            layout='responsive'
          />
        </Grid>
        <Grid
          item
          xl={6}
          lg={6}
          md={7}
          sm={7}
          xs={12}
          sx={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            textAlign: medium ? 'center' : 'left'
          }}
        >
          <Container>
            <Typography variant='h4' sx={{ fontWeight: 'bold' }} mb={1} mt={3}>
              Place Order
            </Typography>
            <Typography
              variant='body1'
              sx={{ textAlign: medium ? 'justify' : null }}
            >
              {' '}
              Ordering in our freight app is simple. Enter shipment details and
              get a cost estimate. Choose a service and confirm your booking,
              then schedule a pickup. Track your shipment, receive delivery
              updates, and finalize payment. Streamlined and user-friendly, our
              app guides you through the process effortlessly.
            </Typography>
          </Container>
        </Grid>
      </Grid>
    </>
  )
}

export default PlaceSection
